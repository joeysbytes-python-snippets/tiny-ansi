# Tiny ANSI

## Clear Screen

Calling clear() will clear the screen and move the cursor to the top left corner.

## Highlight

highlight() takes given text and surrounds it with ANSI codes for decoration. It returns the completed string.

### Parameters

| Parameter | Default Value | Description                                                      |
|-----------|---------------|------------------------------------------------------------------|
| text      | n/a           | The text to surround with ANSI codes                             |
| fg        | None          | Foreground color index: 0 - 15, or None                          |
| bg        | None          | Background color index: 0 - 15, or None                          |
| attrib    | None          | Comma-separated string of attributes (case-insensitive), or None |

### Example Usage

```python
print(highlight("Combine most of the things", fg=11, bg=1, attrib="bold,ital,und,blink,strike,rev"))
```

### ANSI Color Chart

| Index | Color   | Index | Color               |
|-------|---------|-------|---------------------|
| 0     | Black   | 8     | Grey (Bright Black) |
| 1     | Red     | 9     | Bright Red          |
| 2     | Green   | 10    | Bright Green        |
| 3     | Yellow  | 11    | Bright Yellow       |
| 4     | Blue    | 12    | Bright Blue         |
| 5     | Magenta | 13    | Bright Magenta      |
| 6     | Cyan    | 14    | Bright Cyan         |
| 7     | White   | 15    | Bright White        |

### Attributes Chart

| Attribute Values                     | Description                |
|--------------------------------------|----------------------------|
| b, bold, bright                      | Bold (or Bright) Intensity |
| i, ital, italic                      | Italic                     |
| u, und, underline                    | Underline                  |
| d, dim, f, faint                     | Dim (or Faint) Intensity   |
| n, nrml, norm, normal                | Normal Intensity           |
| l, blink                             | Blink                      |
| s, strike, strikethru, strikethrough | Strike-through             |
| r, rev, reverse                      | Reverse Colors             |
