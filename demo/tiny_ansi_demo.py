from typing import Tuple

from tiny_ansi import highlight, clear


def main() -> None:
    clear()
    print_color_chart()
    pause()
    clear()
    print_attributes()
    print()
    print_combo()
    print()


def pause():
    print()
    input(highlight("Press Enter to continue...", fg=0, bg=6))


def print_color_chart() -> None:
    print("Color Chart")
    print("-----------")
    print()
    print("bg: fg -->")
    for bg in range(0, 16):
        print(f"{bg:0>2}: ", end="")
        for fg in range(0, 16):
            print(highlight(f"{fg:0>2} ", fg=fg, bg=bg), end="")
        print()


def print_attributes() -> None:
    print("Attributes")
    print("-----------")
    print()
    _print_attribs(("b", "bold"))
    _print_attribs(("i", "ital", "italic"))
    _print_attribs(("u", "und", "underline"))
    _print_attribs(("d", "dim", "f", "faint"))
    _print_attribs(("n", "nrml", "norm", "normal"))
    _print_attribs(("l", "blink"))
    _print_attribs(("s", "strike", "strikethru", "strikethrough"))
    _print_attribs(("r", "rev", "reverse"))


def _print_attribs(attribs: Tuple) -> None:
    for a in attribs:
        print(highlight(a, attrib=a), end="")
        print(" | ", end="")
    print()


def print_combo() -> None:
    print(highlight("Combine most of the things", fg=11, bg=1, attrib="bold,ital,und,blink,strike,rev"))


if __name__ == "__main__":
    main()
