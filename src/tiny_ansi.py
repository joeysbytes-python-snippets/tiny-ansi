# MIT License: https://gitlab.com/joeysbytes-python-snippets/tiny-ansi/-/raw/main/LICENSE?ref_type=heads
# Copyright (c) 2024 Joey Rockhold

from typing import Optional


def highlight(text: str, fg: Optional[int]=None, bg: Optional[int]=None, attrib: Optional[str]=None) -> str:
    start="\033["; end="\033["; attributes=[] if attrib is None else [a.lower().strip() for a in attrib.split(",")]
    if (fg is not None) and (0 <= fg <= 15): start += f"{fg % 8 + 30 + (60 * (fg > 7))};"; end += "39;"
    if (bg is not None) and (0 <= bg <= 15): start += f"{bg % 8 + 40 + (60 * (bg > 7))};"; end += "49;"
    for a in attributes:
        if a in ("b", "bold", "bright"): start += "22;1;"; end += "22;";
        elif a in ("i", "ital", "italic"): start += "3;"; end += "23;"
        elif a in ("u", "und", "underline"): start += "4;"; end += "24;"
        elif a in ("d", "dim", "f", "faint"): start += "22;2;"; end += "22;"
        elif a in ("n", "nrml", "norm", "normal"): start += "22;"; end += "22;"
        elif a in ("l", "blink"): start += "5;"; end += "25;"
        elif a in ("s", "strike", "strikethru", "strikethrough"): start += "9;"; end += "29;"
        elif a in ("r", "rev", "reverse"): start += "7;"; end += "27;"
    start = f"{start[:-1]}m"; end = f"{end[:-1]}m"; return f"{start}{text}{end}"


def clear():
    print("\033[2J\033[1;1H", end="")
