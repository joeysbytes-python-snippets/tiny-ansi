import pytest
from tiny_ansi import highlight, clear
from data import *


@pytest.mark.parametrize("index, fg", FG_ANSI)
def test_highlight_fg_color(index, fg):
    expected = f"{CSI}{fg}m{TEXT}{CSI}{FG_DEFAULT_ANSI}m"
    actual = highlight(TEXT, fg=index)
    assert actual == expected


@pytest.mark.parametrize("index, fg", FG_ANSI)
def test_highlight_fg_color_multi_code(index, fg):
    expected = f"{CSI}{fg};22m{TEXT}{CSI}{FG_DEFAULT_ANSI};22m"
    actual = highlight(TEXT, fg=index, attrib="normal")
    assert actual == expected


@pytest.mark.parametrize("index, bg", BG_ANSI)
def test_highlight_bg_color(index, bg):
    expected = f"{CSI}{bg}m{TEXT}{CSI}{BG_DEFAULT_ANSI}m"
    actual = highlight(TEXT, bg=index)
    assert actual == expected


@pytest.mark.parametrize("index, bg", BG_ANSI)
def test_highlight_bg_color_multi_code(index, bg):
    expected = f"{CSI}{bg};22m{TEXT}{CSI}{BG_DEFAULT_ANSI};22m"
    actual = highlight(TEXT, bg=index, attrib="normal")
    assert actual == expected


@pytest.mark.parametrize("attrib, start, end", ATTRIB_ANSI)
def test_highlight_attrib(attrib, start, end):
    expected = f"{CSI}{start}m{TEXT}{CSI}{end}m"
    actual = highlight(TEXT, attrib=attrib)
    assert actual == expected


@pytest.mark.parametrize("attrib, start, end", ATTRIB_ANSI)
def test_highlight_attrib_multi_code(attrib, start, end):
    expected = f"{CSI}{start};22m{TEXT}{CSI}{end};22m"
    attrib += ",normal"
    actual = highlight(TEXT, attrib=attrib)
    assert actual == expected


def test_clear(capsys):
    expected = CLEAR_ANSI
    clear()
    captured = capsys.readouterr()
    actual = captured.out
    assert actual == expected
